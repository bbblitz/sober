function htmlEscape(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/\//g, '&#x2F;');
}

function loadLastBugs() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var parts = xhttp.response.split("\n");
      console.log(parts);
      var i = 0
      var table = document.getElementById("bugstable");
      while(i < (parts.length-2)){
        var newrow = document.createElement("tr");
        var submitter = htmlEscape(parts[i]);
        var submitterid = htmlEscape(parts[i+1]);
        var date = htmlEscape(parts[i+2]);
        var desc = htmlEscape(parts[i+3]);
        var status = htmlEscape(parts[i+4]);
        var comments = htmlEscape(parts[i+5]);
        var bugid = htmlEscape(parts[i+6]);
        if(bugid == null){
          return;
        }
        var statusicon = ""
        if(status == "0:"){
          statusicon = "<span class=\"label label-default\">Unassigned</span>";
        }else if(status.substring(0,2) == "1:"){
          statusicon = "<span class=\"label label-info\">" + status.substring(2) + "</span>";
        }else if(status == "2:"){
          statusicon = "<span class=\"label label-success\">Closed</span>";
        }
        newrow.innerHTML = "<tr><td><a href=\"./bugview.html?id=" + bugid + "\">View<span class=\"badge\">" + comments + "</span></a></td><td>" + submitter + "<b>" + submitterid + "</b>" + "</td><td>" + date + "</td><td>" + desc + "</td><td>" + statusicon + "</td></tr>";
        table.appendChild(newrow);
        i = i + 7;
      }
    }
  };
  xhttp.open("GET", "./cgi-bin/bugsdata.cgi", true);
  xhttp.send();
  var captcha = new XMLHttpRequest();
  captcha.onreadystatechange = function() {
    if (captcha.readyState == 4 && captcha.status == 200) {
      var id = captcha.response;
      document.getElementById("captcha").innerHTML = "<img src=\"./captchas/" + id + ".png\">";
    }
  }
  captcha.open("GET", "./cgi-bin/gencaptcha.cgi",true);
  captcha.send();
}
window.onload = loadLastBugs
