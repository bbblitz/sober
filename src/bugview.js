function getQueryStrings() {
  var assoc  = {};
  var decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); };
  var queryString = location.search.substring(1);
  var keyValues = queryString.split('&');

  for(var i in keyValues) {
    var key = keyValues[i].split('=');
    if (key.length > 1) {
      assoc[decode(key[0])] = decode(key[1]);
    }
  }

  return assoc;
}

function htmlEscape(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/\//g, '&#x2F;')
        .replace(/\\n/g, '<br/>');
}

function loadLastBugs() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var parts = xhttp.response.split("\n");
      var query = getQueryStrings();
      var bugnum = query["id"];
      document.getElementById("submitter").innerHTML = htmlEscape(parts[0]);
      document.getElementById("subid").innerHTML = htmlEscape(parts[1]);
      document.getElementById("date").innerHTML = htmlEscape(parts[2]);
      document.getElementById("bugid").innerHTML = "Bug #" + bugnum;
      document.getElementById("shortdesc").innerHTML = htmlEscape(parts[3]);
      var statustext = parts[4];
      var statusicon = ""
      if(statustext == "0:"){
        statusicon = "<span class=\"label label-default\">Unassigned</span>";
      }else if(statustext.substring(0,2) == "1:"){
        statusicon = "<span class=\"label label-info\">" + status.substring(2) + "</span>";
      }else if(statustext == "2:"){
        statusicon = "<span class=\"label label-success\">Closed</span>";
      }

      document.getElementById("status").innerHTML = statusicon;
      document.getElementById("longdesc").innerHTML = htmlEscape(parts[5]);
      console.log(parts);
      var i = 6;
      var commentsec = document.getElementById("comments");
      while(i < (parts.length-2)){
        var commentnode = document.createElement("div");
        commentnode.setAttribute("class","panel panel-info");
        var commenter = htmlEscape(parts[i+1]);
        var commentid = htmlEscape(parts[i+2]);
        var commenttext = htmlEscape(parts[i+3]);
        if(commenttext == null){
          return;
        }
        commentnode.innerHTML = "<div class=\"panel-heading\">" + commenter + "<b>" + commentid + "</b></div><div class=\"panel-body\">" + commenttext + "</div>";
        commentsec.appendChild(commentnode);
        i = i + 4;
      }
      //document.getElementById("longdesc").innerHTML = xhttp.responseText;
    }
  };
  var query = getQueryStrings();
  var bugnum = query["id"];
  xhttp.open("GET", "./cgi-bin/showbug.cgi?id="+bugnum, true);
  xhttp.send();
  var captcha = new XMLHttpRequest();
  captcha.onreadystatechange = function() {
    if (captcha.readyState == 4 && captcha.status == 200) {
      var id = captcha.response;
      document.getElementById("captcha").innerHTML = "<img src=\"./captchas/" + id + ".png\">";
      var query = getQueryStrings();
      var bugnum = query["id"];
      document.getElementById("bgid").setAttribute("value",bugnum);
      console.log("Added hidden field to form:" + bugnum);
    }
  }
  captcha.open("GET", "./cgi-bin/gencaptcha.cgi",true);
  captcha.send();
}
window.onload = loadLastBugs
