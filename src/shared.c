//this file contains functions that all other files occasinally need.
//They are collected here to make bugfixes eaiser.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "shared.h"
#include "config.h"

char* replaceHTML(char* str){
  //printf("Replacehtml");
  char* nstr = malloc((sizeof(char)*strlen(str)) + 1);
  if(nstr == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Failed to allocate memory!\n");
    exit(0);
  }

  int strp = 0;
  int nstrp = 0;
  while(str[strp] != '\0'){
    //printf("Strp=%d nstrp=%d\n",strp,nstrp);
    if(str[strp] == '+'){
      nstr[nstrp] = ' ';
      strp++;
      nstrp++;
    }else if(str[strp] == '%'){
      //printf("Hit encodeing!\n");
      int hex = 0;
      sscanf(str+strp,"%%%2x",&hex);
      //printf("Was:%c(%d)\n",(char)hex,hex);
      if(hex == 0x0D){
        strp+=3;
        continue;
      }
      if(hex == 0x0A){
        nstr[nstrp] = '\\';
        nstr[nstrp+1] = 'n';
        nstrp+=2;
        strp+=3;
        continue;
      }
      nstr[nstrp] = (char)hex;
      nstrp++;
      strp+=3;
    }else if(str[strp] == '\n'){
      nstr[nstrp] = '\\';
      nstr[nstrp+1] = 'n';
      nstrp+=2;
      strp++;
    }else{
      nstr[nstrp] = str[strp];
      nstrp++;
      strp++;
    }
  }
  nstr[nstrp] = '\0';
  //printf("HTML replaced:%s",nstr);
  return nstr;
}

unsigned long hash(unsigned char *str){
  unsigned long hash = 5381;
  int c;

  while (c = *str++)
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

  return hash;
}

char* useridhash(char* str){
  //printf("useridhash\n");
  char* hash = calloc(sizeof(char)*20,sizeof(char));
  int i = (int)str[0];
  int len = strlen(str);
  int tloop = i*20;
  unsigned int hashp = 0;
  unsigned int strp = 0;
  i = 0;
  while(i < tloop){
    //printf("i:%d hashp:%u strp:%u tloop:%d\n",i,hashp,strp,tloop);
    char tchar = str[strp%len];
    hash[hashp%20] += str[strp%len];
    str[strp%len]+=1;
    hashp+=(int)tchar;
    strp+=(int)(hashp*hashp);
    i++;
  }
  i = 0;
  //printf("Before characterizeing the hash, it was: %s\n",hash);
  while(i < 20){
    //printf("Normalizeing %c(%u) as %c(%u)\n",hash[i],(unsigned int)hash[i],(hash[i] % 92) + 32,(hash[i] % 92) + 32);
    unsigned int hashnum = hash[i];
    unsigned int modedhashnum = hashnum % 92;
    //printf("hashnum was %u, after mod it is:%u\n",hashnum,modedhashnum);
    hash[i] = modedhashnum + 32;
    i++;
  }
  //printf("Resulting hash was:%s\n",hash);
  return hash;
}

void makeRecent(unsigned long long bugid){
  unsigned long long recent[20];
  int zerocount = 0;
  while(zerocount < 20){
    recent[zerocount++] = 0;
  }
  char recentfilepath[100];
  sprintf(recentfilepath,"%s/bugs/recent",REL_BINPATH);
  FILE* recentfile;
  recentfile = fopen(recentfilepath,"r+");
  if(recentfile == NULL){
    printf("Failed to open recent bugs file!\n");
    return;
  }
  char c;
  char thisnum[10];
  int tp = 0;
  int rp = 0;
  while(!feof(recentfile) && (c = fgetc(recentfile)) && c != EOF){
    if(c == '\n'){
      thisnum[tp] = '\0';
      sscanf(thisnum,"%10llu",&(recent[rp]));
      rp++;
      tp = 0;
    }else{
      thisnum[tp] = c;
      tp++;
    }
  }
  fclose(recentfile);

  int i = 19;
  while(i > 0){
    recent[i] = recent[i-1];
    i--;
  }
  recent[0] = bugid;

  FILE* nfile;
  nfile = fopen(recentfilepath,"w");
  fprintf(nfile,"%llu",recent[0]);
  i = 1;
  while(i < 20){
    fprintf(nfile,"\n%llu",recent[i]);
    i++;
  }
  fclose(nfile);
}

void printbug(FILE* f){

  int line = 0;
  char tchar = ' ';
  while((tchar = fgetc(f)) && tchar != EOF){
    if(tchar == '\n')
      line++;
    if(line > 4)
      break;
    putchar(tchar);
  }

  unsigned int comments = 0;
  while (EOF != (fscanf(f,"%*[^\n]"), fscanf(f,"%*c")))
    ++comments;
  printf("\n%u\n",(comments-1)/4);
}
