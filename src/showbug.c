#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "shared.h"
#include "config.h"

int main(){
  char* data = getenv("QUERY_STRING");
  //char data[20] = "?id=1";
  char* hardip = getenv("REMOTE_ADDR");
  printf("%s%c%c\n",
  "Content-Type:text/html;charset=iso-8859-1",13,10);
  FILE* lastfilenum;
  char lastfilenumpath[64];
  sprintf(lastfilenumpath,"%s/bugs/lastbug",REL_BINPATH);
  lastfilenum = fopen(lastfilenumpath,"r");
  if(lastfilenum == NULL){
    printf("Error reteriveing bugs, contact the admin!");
    return 1;
  }
  long long last = 0;
  if(fscanf(lastfilenum,"%lld",&last) < 1){
    printf("Error finding file id");
    return 1;
  }
  fclose(lastfilenum);
  long long thisid = 0;
  sscanf(data,"id=%lld",&thisid);
  if(thisid > last){
    printf("Invalid!");
    return 1;
  }

  char filestring[64];
  sprintf(filestring,"%s/bugs/",REL_BINPATH);
  char filename[10];
  sprintf(filename,"%lld",thisid);
  strcat(filestring,filename);
  FILE* bugfile;
  bugfile = fopen(filestring,"r");
  if(bugfile == NULL){
    printf("Unable to open bug file!");
    return 1;
  }
  while(!feof(bugfile)){
    char c = fgetc(bugfile);
    if(feof(bugfile)){
      break;
    }else{
      putchar(c);
    }
  }
  fclose(bugfile);
  return 0;
}
