#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "config.h"

void printbug(FILE* f);

int main(){
  printf("%s%c%c\n",
  "Content-Type:text/html;charset=iso-8859-1",13,10);
  FILE* lastfilenum;
  char lastfilenumpath[100];
  sprintf(lastfilenumpath,"%s/bugs/lastbug",REL_BINPATH);
  lastfilenum = fopen(lastfilenumpath,"r");
  if(lastfilenum == NULL){
    printf("Error reteriveing bugs, contact the admin!");
    return 1;
  }
  long long last = 0;
  fscanf(lastfilenum,"%lld",&last);
  fclose(lastfilenum);
  long long end = last-20;

  while(last > end && last > 0){
    char filestring[64];
    sprintf(filestring,"%s/bugs/",REL_BINPATH);
    char filename[10];
    sprintf(filename,"%lld",last);
    strcat(filestring,filename);
    FILE* thisbug;
    thisbug = fopen(filestring,"r");
    if(thisbug == NULL){
      printf("Error opening bug:%s\n",filestring);
      printf("<br/>");
      last--;
      continue;
    }
    printbug(thisbug);
    printf("%lld\n",last);
    fclose(thisbug);
    last--;
  }

  return 0;
}
