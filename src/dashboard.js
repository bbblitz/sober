function loadDashboard() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var parts = xhttp.response
      document.getElementById("text").innerHTML = parts
    }
  };
  //var query = getQueryStrings();
  //var bugnum = query["id"];
  xhttp.open("GET", "./cgi-bin/dashboard.cgi?cookie="+document.cookie, true);
  xhttp.send();
}
var lastcheckin;
function setup(){
  loadDashboard();
  lastchecking = getMinutes();
}
window.onload = setup

if (!("Notification" in window)) {
  alert("This browser does not support desktop notification");
}
else if (Notification.permission !== 'denied') {
  Notification.requestPermission(function (permission) {
  });
}

function checkfornewbugs(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var parts = xhttp.response
      document.getElementById("notification").innerHTML = parts
    }
  };
  //var query = getQueryStrings();
  //var bugnum = query["id"];
  xhttp.open("GET", "./cgi-bin/popups.cgi?for=" + document.cookie, true);
  xhttp.send();
}

function notifyMe() {
  var title = "This is a title!";
  var options = {
    body: "Some body text",
    data: "This is data",
  };
  var notification = new Notification("Hi there!", options);
  notification.onclick = function(){
    alert("You clicked: " + notification.data);
  }
}
