#include <stdio.h>
#include <stdlib.h>
#include "shared.h"
#include "config.h"

int main(){
  //char* data = getenv("QUERY_STRING");
  char data[] = "user=Apickx";
  char user[30];
  sscanf(data,"user=%29s",user);
  char recentbugspath[100];
  sprintf(recentbugspath,"%s/bugs/recent",REL_BINPATH);
  FILE* recentfile;
  recentfile = fopen(recentbugspath,"r");
  if(recentfile == NULL){
    printf("Unable to open recent file");
  }
  int recentline = 0;
  while(recentline < 20){
    char bugnumstring[10];
    int bnsp = 0;
    char c;
    while(!feof(recentfile) && (c = fgetc(recentfile)) && c!=EOF && c!='\n'){
      bugnumstring[bnsp] = c;
      bnsp++;
    }
    bugnumstring[bnsp] = '\0';
    unsigned long long bugnum;
    sscanf(bugnumstring,"%llu",&bugnum);
    char thisbugfilepath[100];
    sprintf(thisbugfilepath,"%s/bugs/%llu",REL_BINPATH,bugnum);
    FILE* thisbugfile;
    thisbugfile = fopen(thisbugfilepath,"r");
    int bugfileline = 1;
    while(bugfileline != 5){
      char c = fgetc(thisbugfile);
      if(c == '\n'){
        bugfileline++;
      }
    }
    char assignee[64];
    int type;
    fscanf(thisbugfile,"%d:%s\n",&type,assignee);
    if(type == 1){
      if(strcmp(assignee,user) == 0){
        printf("I found a bug for me! Num:%d\n",bugnum);
      }
    }


    recentline++;
  }
}
