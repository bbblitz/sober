#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "shared.h"
#include "config.h"

#define ADJNUM 200
#define NOUNUM 1524
#define AVGADJ 7
#define AVGNOU 7
#define LONGADJ 14
#define LONGNOU 15

void createRandomWords(char* buf){
  long adjnum = rand() % ADJNUM;
  long nounum = rand() % NOUNUM;
  char adjfilepath[64];
  char noufilepath[64];
  sprintf(adjfilepath,"%s/adj.txt",REL_BINPATH);
  sprintf(noufilepath,"%s/nouns.txt",REL_BINPATH);
  FILE* adjfile;
  FILE* noufile;
  adjfile = fopen(adjfilepath,"r");
  noufile = fopen(noufilepath,"r");
  //Instead of getting a particular line, seek an average length, and find the next word.
  if(adjfile == NULL || noufile == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Something has gone serirously wrong!");
    exit(1);
  }
  fseek(adjfile,adjnum*AVGADJ,0);
  fseek(noufile,nounum*AVGNOU,0);

  while(fgetc(adjfile) != '\n'){}
  while(fgetc(noufile) != '\n'){}
  int pos = 0;
  char tchar = fgetc(adjfile);
  do{
    buf[pos] = tchar;
    pos++;
    tchar = fgetc(adjfile);
  }while(tchar != '\n');
  buf[pos] = ' ';
  pos++;
  tchar = fgetc(noufile);
  do{
    buf[pos] = tchar;
    pos++;
    tchar = fgetc(noufile);
  }while(tchar != '\n');
  buf[pos] = '\0';
}

int main(){

  srand(time(NULL));
  char randwords[LONGADJ+LONGNOU+1];
  createRandomWords(randwords);

  int barraldis1 = rand()%10;
  int barraldis2 = rand()%10;
  int arcdis = (rand()%45)+45;
  char command[1024];
  unsigned long rhash = hash(randwords);
  sprintf(command,"convert -background white -fill black -pointsize 48 label:\"%s\" \"%s/captchas/%lu.png\"",randwords,REL_BINPATH,rhash);
  system(command);
  sprintf(command,"convert \"%s/captchas/%lu.png\" -distort Barrel \"0.0%d 0.0 0.0%d\" \"%s/captchas/%lu.png\"",REL_BINPATH,rhash,barraldis1,barraldis2,REL_BINPATH,rhash);
  system(command);
  sprintf(command,"convert \"%s/captchas/%lu.png\" -virtual-pixel White -distort Arc %d \"%s/captchas/%lu.png\"",REL_BINPATH,rhash,arcdis,REL_BINPATH,rhash);
  system(command);

  printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
  printf("%lu",rhash);

  FILE* tfile;
  char filepath[100];
  sprintf(filepath,"%s/captchas/%s.txt",REL_BINPATH,randwords);
  tfile = fopen(filepath,"w");
  fprintf(tfile,"%lu",rhash);
  fclose(tfile);

  return 0;
}
