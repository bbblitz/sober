#include <stdio.h>
#include <stdlib.h>
#include "shared.h"
#include "config.h"

int main(){
  char* data = getenv("QUERY_STRING");
  //char data[] = "user=Apickx";
  char user[30];
  sscanf(data,"user=%29s",user);
  char* duser = replaceHTML(user);
  char lastbugfilepath[100];
  sprintf(lastbugfilepath,"%s/bugs/lastbug",REL_BINPATH);
  FILE* lastbugfile;
  lastbugfile = fopen(lastbugfilepath,"r");
  if(lastbugfile == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Unable to open recent file");
    exit(1);
  }
  unsigned long long lastbugnum;
  fscanf(lastbugfile,"%llu",&lastbugnum);
  fclose(lastbugfile);
  unsigned long long thisbugnum = lastbugnum;
  printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
  while(thisbugnum > 0){
    char thisbugpath[100];
    sprintf(thisbugpath,"%s/bugs/%llu",REL_BINPATH,thisbugnum);
    FILE* thisbugfile;
    thisbugfile = fopen(thisbugpath,"r");
    int bugfileline = 1;
    while(bugfileline != 5){
      char c = fgetc(thisbugfile);
      if(c == '\n'){
        bugfileline++;
      }
    }
    char assignee[64];
    int type;
    fscanf(thisbugfile,"%d:%s\n",&type,assignee);
    if(type == 1){
      if(strcmp(assignee,duser) == 0){
        //If this is a file we need to print, back up and print it
        fseek(thisbugfile,0,SEEK_SET);
        printbug(thisbugfile);
      }
    }
    fclose(thisbugfile);
    thisbugnum--;
  }
}
