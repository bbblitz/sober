#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

char* replaceHTML(char* str){
  //printf("Replaceing:%s\n",str);
  char* nstr = malloc((sizeof(char)*strlen(str)) + 1);
  if(nstr == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Failed to allocate memory!\n");
  }

  int strp = 0;
  int nstrp = 0;
  while(str[strp] != '\0'){
    //printf("Strp=%d nstrp=%d\n",strp,nstrp);
    if(str[strp] == '+'){
      nstr[nstrp] = ' ';
      strp++;
      nstrp++;
    }else if(str[strp] == '%'){
      //printf("Hit encodeing!\n");
      int hex = 0;
      sscanf(str+strp,"%%%2x",&hex);
      //printf("Was:%c(%d)\n",(char)hex,hex);
      if(hex == 0x0D){
        strp+=3;
        continue;
      }
      if(hex == 0x0A){
        nstr[nstrp] = '\\';
        nstr[nstrp+1] = 'n';
        nstrp+=2;
        strp+=3;
        continue;
      }
      nstr[nstrp] = (char)hex;
      nstrp++;
      strp+=3;
    }else if(str[strp] == '\n'){
      nstr[nstrp] = '\\';
      nstr[nstrp+1] = 'n';
      nstrp+=2;
      strp++;
    }else{
      nstr[nstrp] = str[strp];
      nstrp++;
      strp++;
    }
  }
  nstr[nstrp] = '\0';
  //printf("Done, str is now:%s\n",nstr);
  return nstr;
}

char* useridhash(char* str){
  char* hash = calloc(sizeof(char)*20,sizeof(char));
  int i = (int)str[0];
  int len = strlen(str);
  int tloop = i*20;
  unsigned int hashp = 0;
  unsigned int strp = 0;
  i = 0;
  while(i < tloop){
    printf("i:%d hashp:%u strp:%u tloop:%d\n",i,hashp,strp,tloop);
    char tchar = str[strp%len];
    hash[hashp%20] += str[strp%len];
    str[strp%len]+=1;
    hashp+=(int)tchar;
    strp+=(int)(hashp*hashp);
    i++;
  }
  i = 0;
  printf("Before characterizeing the hash, it was: %s\n",hash);
  while(i < 20){
    //printf("Normalizeing %c(%u) as %c(%u)\n",hash[i],(unsigned int)hash[i],(hash[i] % 92) + 32,(hash[i] % 92) + 32);
    unsigned int hashnum = hash[i];
    unsigned int modedhashnum = hashnum % 92;
    //printf("hashnum was %u, after mod it is:%u\n",hashnum,modedhashnum);
    hash[i] = modedhashnum + 32;
    i++;
  }
  printf("Resulting hash was:%s\n",hash);
  return hash;
}

int main(){
  char teststring[] = "Test";
  printf("Original:%s\n",teststring);
  char* parsed = useridhash(teststring);
  printf("%s\n",parsed);
/*
  char teststring2[] = "Tost";
  printf("Original:%s\n",teststring2);
  char* parsed2 = useridhash(teststring2);
  printf("%s\n",parsed2);

  char teststring3[]  = "This%0D%0AIs%0D%0AAnother%0D%0AMulti%0D%0ALine%0D%0AComment";
  printf("Original:%s\n",teststring3);
  char* parsed3 = useridhash(teststring3);
  printf("%s\n",parsed3);
  free(parsed);
  free(parsed2);
  free(parsed3);
  return 0;
*/
}
