#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "config.h"
#include "shared.h"

int main(){
  char* data = getenv("QUERY_STRING");
  char* hardip = getenv("REMOTE_ADDR");
  //char data[] = "name=Apickx&id=test&comment=Can+I+comment+on+other+bugs%3F&captcha=clean+night&bugid=7";

  char name[15];
  char userid[20];
  char comment[2048];
  char captcha[30];
  char bugid[5];

  char* iname = strtok(data,"&");
  char* iuserid = strtok(NULL,"&");
  char* icomment = strtok(NULL,"&");
  char* icaptcha = strtok(NULL,"&");
  char* ibugid = strtok(NULL,"&");

  sscanf(iname,"name=%s",name);
  sscanf(iuserid,"id=%s",userid);
  sscanf(icomment,"comment=%s",comment);
  sscanf(icaptcha,"captcha=%s",captcha);
  sscanf(ibugid,"bugid=%s",bugid);

  //Check captcha
  char captchapath[100];
  char* captchadecoded = replaceHTML(captcha);
  sprintf(captchapath,"%s/captchas/%s.txt",REL_BINPATH,captchadecoded);
  FILE* captchafile = fopen(captchapath,"r");
  if(captchafile == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Captcha incorrect");
    return 0;
  }
  unsigned long inputhash = hash(captchadecoded);
  free(captchadecoded);
  unsigned long filehash = 0;
  fscanf(captchafile,"%lu",&filehash);
  if(filehash != inputhash){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Captcha incorrect");
    return 0;
  }
  fclose(captchafile);
  //If the program got here, the captcha was correct

  //Delete the captcha file so you can't use the same captcha twice
  char command[100];
  sprintf(command,"rm \"%s\"",captchapath);
  system(command);

  sprintf(command,"rm \"%s/captchas/%lu.png\"",REL_BINPATH,inputhash);
  system(command);

  //Add comment to bug file
  char filepath[100];
  sprintf(filepath,"%s/bugs/%s",REL_BINPATH,bugid);
  FILE* bugfile = fopen(filepath,"a");
  if(bugfile == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("<p>Unable to find bug!");
    return 0;
  }
  char* dname = replaceHTML(name);
  char* duid = useridhash(userid);
  char* dcomment = replaceHTML(comment);

  //Redirect the user
  printf("Location: %s/bugview.html?id=%s\n\n",REL_BINPATH,bugid);

  //And create the bug
  fprintf(bugfile,"\n%s\n%s\n%s\n",dname,duid,dcomment);
  fclose(bugfile);

  //Make this bug recent, so everyone gets a notification
  unsigned long long nbugid = 0;
  sscanf(bugid,"%llu",&nbugid);
  makeRecent(nbugid);

  free(dname);
  free(duid);
  free(dcomment);
  return 0;
}
