#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "shared.h"
#include "config.h"
#include <time.h>

int main(){
  char* data = getenv("QUERY_STRING");
  char* hardip = getenv("REMOTE_ADDR");
  //char data[] = "name=Apickx&id=test&shortdesc=This+is+a+short+description+of+the+bug&longdesc=This+is+a+much+longer+description+of+the+bug!&captcha=scary+grocery";

  char name[20];
  char userid[20];
  char shortdesc[128];
  char longdesc[4096];
  char captcha[30];
//http://cogarr.net/bugs/cgi-bin/newbug.cgi?name=Apickx&id=test&shortdesc=This+is+a+short+description+of+the+bug&longdesc=This+is+a+much+longer+description+of+the+bug!&captcha=victorious+capital
  char* iname = strtok(data,"&");
  char* iuserid = strtok(NULL,"&");
  char* ishortdesc = strtok(NULL,"&");
  char* ilongdesc = strtok(NULL,"&");
  char* icaptcha = strtok(NULL,"&");

  sscanf(iname,"name=%19s",name);
  sscanf(iuserid,"id=%19s",userid);
  sscanf(ishortdesc,"shortdesc=%127s",shortdesc);
  sscanf(ilongdesc,"longdesc=%4095s",longdesc);
  sscanf(icaptcha,"captcha=%29s",captcha);

  //printf("name:%s\nid:%s\nshort:%s\nlong:%s\ncaptcha:%s\n",name,userid,shortdesc,longdesc,captcha);

  //Check captcha
  char captchapath[100];
  char* captchadecoded = replaceHTML(captcha);
  sprintf(captchapath,"%s/captchas/%s.txt",REL_BINPATH,captchadecoded);

  FILE* captchafile = fopen(captchapath,"r");
  if(captchafile == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Captcha incorrect:No file found, %s, %s, %s, %s",captchapath, captchadecoded, captcha, icaptcha);
    return 1;
  }
  unsigned long inputhash = hash(captchadecoded);
  free(captchadecoded);
  unsigned long filehash = 0;
  fscanf(captchafile,"%lu",&filehash);
  if(filehash != inputhash){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Captcha incorrect:Bad hash");
    return 1;
  }

  //If we had the correct captcha, remove it.
  char command[100];
  sprintf(command,"rm \"%s\"",captchapath);
  system(command);

  //Create new bug

  //Find out what bug number this new bug is
  char lastbugfilepath[64];
  sprintf(lastbugfilepath,"%s/bugs/lastbug",REL_BINPATH);
  FILE* lastbugfile = fopen(lastbugfilepath,"r+");
  if(lastbugfile == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Unable to open LastBugFile!");
    return 1;
  }
  unsigned long long lastbug = 0;
  fscanf(lastbugfile,"%llu",&lastbug);
  fseek(lastbugfile,0,SEEK_SET);
  lastbug++;
  fprintf(lastbugfile,"%llu",lastbug);
  fclose(lastbugfile);

  //And then create the new file, with it's data
  char* dname = replaceHTML(name);
  char* duid = useridhash(userid);
  time_t rawtime;
  struct tm * timeinfo;
  time(&rawtime);
  timeinfo = (struct tm*) localtime(&rawtime);
  char* dshortd = replaceHTML(shortdesc);
  char* dlongd = replaceHTML(longdesc);
  printf("Location: %s/bugview.html?id=%llu\n\n",REL_BINPATH,lastbug);
  //printf("Right before printing to file\n");
  char newfilepath[100];
  sprintf(newfilepath,"%s/bugs/%llu",REL_BINPATH,lastbug);
  FILE* newfile = fopen(newfilepath,"w");
  if(newfile == NULL){
    printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
    printf("Error createing new bug file");
  }
  fprintf(newfile,"%s\n%s\n%s%s\n0:\n%s\n",dname,duid,asctime(timeinfo),dshortd,dlongd);
  fclose(newfile);

  //finally, make this bug recent so everyone will receive a notification
  makeRecent(lastbug);
  
  free(dname);
  free(duid);
  free(dshortd);
  free(dlongd);

  return 0;
}
