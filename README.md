# Simple Open Bug and Error Reporter

SOBER is a smimple web-based bug reporter. It is written in ANSI C and should work on any posix-compliant platform & web server with cgi support.

You can find a demo instance at [cogarr.net/demos/sober](https://cogarr.net/demos/sober)

## Installation

To install, 
1. clone this repo
2. edit config.h 
3. edit the first 3 lines of build.bat or build.sh
4. run the build.bat or build.sh script.
5. point your web server to the cgi-bin directory.
SOBER should set up it's files as needed.

## Usage
People that need to use the bug tracker should have access (ex over ssh) to the deploy/bugs directory. Each bug has a id# as a filename. You can search for bugs by using grep. Ex:
`grep "0:" *` will find all open bugs

To give feedback to the users of the site, change the 5th line in the bug report. It has the format <status>:<text>
statis can be:

* 0 : unassigned, text MUST NOT exist
* 1 : assigned, text will be displayed
* 2 : closed, text MUST NOT exist

## Misc
If your filesystem supports it, "lock" the file while it is being edited by a developer, so that data submitted by  users is not overwritten.

