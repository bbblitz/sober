#!/bin/sh

DEPLOYDIR="/var/www/webpage/bugs/artery"
PROJECTNAME="Artery bug tracker"
ADMINEMAIL="alexandermpickering@gmail.com"

##Check to see if imgmagik is installed
convert -version > /dev/null
if [ $? -ne 0 ]; then
  echo "imagemagik not installed!"
  return 1
fi

##Check to see if a c compiler is installed
CC -v > /dev/null
if [ $? -ne 0 ]; then
  echo "C compiler variable not set, checking if GCC is installed"
  gcc -v > /dev/null
  if [ $? -ne 0 ]; then
    echo "No C compiler found! Aborting!";
    return 1
  fi
  CC = "gcc"
fi

##Everything looks good to go! Compile!
cp config.h src/config.h

mkdir "$DEPLOYDIR"
mkdir "$DEPLOYDIR/cgi-bin"
mkdir "$DEPLOYDIR/bugs"
mkdir "$DEPLOYDIR/captchas"

gcc -o "$DEPLOYDIR/cgi-bin/bugcomment.cgi" src/bugcomment.c src/shared.c
gcc -o "$DEPLOYDIR/cgi-bin/bugsdata.cgi" src/bugsdata.c src/shared.c
gcc -o "$DEPLOYDIR/cgi-bin/gencaptcha.cgi" src/gencaptcha.c src/shared.c
gcc -o "$DEPLOYDIR/cgi-bin/newbug.cgi" src/newbug.c src/shared.c
gcc -o "$DEPLOYDIR/cgi-bin/showbug.cgi" src/showbug.c src/shared.c
gcc -o "$DEPLOYDIR/cgi-bin/searchbugs.cgi" src/searchbugs.c src/shared.c
gcc -o "$DEPLOYDIR/cgi-bin/popups.cgi" src/popups.c src/shared.c

cp src/*.html "$DEPLOYDIR/"
cp src/*.txt "$DEPLOYDIR/"
cp src/*.js "$DEPLOYDIR/"
cp src/*.css "$DEPLOYDIR/"
cp -R src/css "$DEPLOYDIR/"

#Since the POSIX shell dosen't specify arrays, you can't simplyfy this to an array, and loop over it
sed -e  "s~SOBER_DEPLOY_DIR~$DEPLOYDIR~g" \
    -e  "s~SOBER_ADMIN_CONTACT~$ADMINEMAIL~g" \
    -e  "s~SOBER_PROJECT_NAME~$PROJECTNAME~g" < src/index.js > "$DEPLOYDIR/index.js"

sed -e  "s~SOBER_DEPLOY_DIR~$DEPLOYDIR~g" \
    -e  "s~SOBER_ADMIN_CONTACT~$ADMINEMAIL~g" \
    -e  "s~SOBER_PROJECT_NAME~$PROJECTNAME~g" < src/index.html > "$DEPLOYDIR/index.html"

sed -e  "s~SOBER_DEPLOY_DIR~$DEPLOYDIR~g" \
    -e  "s~SOBER_ADMIN_CONTACT~$ADMINEMAIL~g" \
    -e  "s~SOBER_PROJECT_NAME~$PROJECTNAME~g" < src/bugview.html > "$DEPLOYDIR/bugview.html"

sed -e  "s~SOBER_DEPLOY_DIR~$DEPLOYDIR~g" \
    -e  "s~SOBER_ADMIN_CONTACT~$ADMINEMAIL~g" \
    -e  "s~SOBER_PROJECT_NAME~$PROJECTNAME~g" < src/bugview.js > "$DEPLOYDIR/bugview.js"

sed -e  "s~SOBER_DEPLOY_DIR~$DEPLOYDIR~g" \
    -e  "s~SOBER_ADMIN_CONTACT~$ADMINEMAIL~g" \
    -e  "s~SOBER_PROJECT_NAME~$PROJECTNAME~g" < src/searchbugs.html > "$DEPLOYDIR/searchbugs.js"

sed -e  "s~SOBER_DEPLOY_DIR~$DEPLOYDIR~g" \
    -e  "s~SOBER_ADMIN_CONTACT~$ADMINEMAIL~g" \
    -e  "s~SOBER_PROJECT_NAME~$PROJECTNAME~g" < src/searchbugs.js > "$DEPLOYDIR/searchbugs.js"

echo 0 > "$DEPLOYDIR/bugs/lastbug"
touch "$DEPLOYDIR/bugs/recent"
chmod -R 777 "$DEPLOYDIR/bugs" "$DEPLOYDIR/captchas"

rm src/config.h
echo "Setup complete!"
#read -n1 -r -p "Press space to continue..." key
