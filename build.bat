
set deploydir=deploy

rem Create the build directory
MKDIR %deploydir%
MKDIR %deploydir%\cgi-bin
MKDIR %deploydir%\bugs
MKDIR %deploydir%\captchas

rem And copy the settings in
COPY config.h src\config.h

rem Compile the CGI scripts
gcc src\bugcomment.c src\shared.c
MOVE a.exe %deploydir%\cgi-bin\bugcomment.cgi
gcc src\bugsdata.c src\shared.c
MOVE a.exe %deploydir%\cgi-bin\bugsdata.cgi
gcc src\gencaptcha.c src\shared.c
MOVE a.exe %deploydir%\cgi-bin\gencaptcha.cgi
gcc src\newbug.c src\shared.c
MOVE a.exe %deploydir%\cgi-bin\newbug.cgi
gcc src\showbug.c src\shared.c
MOVE a.exe %deploydir%\cgi-bin\showbug.cgi

DEL src\config.h

rem Copy the html, javascript, and css files
COPY src\*.txt %deploydir%\
COPY src\*.html %deploydir%\
COPY src\*.js %deploydir%\
COPY src\*.css %deploydir%\

pause
